import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from './../../environments/environment';
@Injectable()
export class ClientService {
  private allPath = {
    // authorization: 'http://172.28.20.55:82',
    // api: 'http://172.28.20.55:82/api/v1'
    api: `${environment.apiUrl}/api/v1`
  };

  constructor(private router: Router) {
  }

  /**
   * Salva o Token de autorização de consumo da API no localstorage.
   */
  public setApiServiceTokenApi(accessToken) {
    return localStorage.setItem('apiServiceToken', accessToken);
  }

  /**
   * Pega o Token de autorização de consumo da API.
   */
  public getApiServiceToken() {
    return localStorage.getItem('apiServiceToken');
  }

  /**
   * Salva o Token de autorização de consumo da API no localstorage.
   */
  public setUserProfile(userData) {
    return new Promise((resolve) => {
      localStorage.setItem('userProfile', userData);
      resolve(userData);
    });
  }

  /**
   * Verifica os dados do usuário se logado.
   */
  public isLogged() {
    return new Promise((resolve) => {
      const logged = localStorage.getItem('userProfile');
      if (logged) {
        resolve(logged);
      } else {
        resolve(logged);
        this.router.navigate(['/login']);
      }
    });
  }

  /**
   * Formata de forma erializa o objeto(necessário) para comunicar-se com a API.
   */
  private formatData(params) {
    const formated = Object.keys(params).map((key) => {
      return key + '=' + params[key];
    }).join('&');
    return formated;
  }

  /**
   * Realiza a criação do comentário.
   */
  public postAll(route, obj, serialized) {
    return new Promise((resolve, reject) => {
      fetch(`${this.allPath.api}/${route}`, {
        method: 'POST',
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'Content-Type': (serialized) ? 'application/x-www-form-urlencoded;charset=UTF-8' : 'application/json',
          'X-Requested-With': 'XMLHttpRequest'
        }),
        body: (serialized) ? this.formatData(obj) : JSON.stringify(obj)
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(e);
          console.error(e);
        });
    });
  }

  /**
   * Busca todos os registros.
   */
  public getAll(route, obj) {
    const finalRoute = (obj == null) ? `${route}` : `${route}/${obj}`;
    return new Promise((resolve, reject) => {
      fetch(`${this.allPath.api}/${finalRoute}`, {
        method: 'GET',
        headers: new Headers({
          Accept: 'application/json',
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'Content-Type': 'application/x-www-form-urlencoded'
        }),
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message));
        });
      });
  }

  /**
   * Busca todos os registros com paginação.
   */
  public getAllPaginate(route, obj, page) {
    return new Promise((resolve, reject) => {
      fetch(`${this.allPath.api}/${route}/${obj}?page=${page}`, {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        }),
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message));
        });
      });
  }

  /**
   * Busca um registro específico pelo ID.
   */
  public getById(route, id) {
    return new Promise((resolve, reject) => {
      fetch(`${this.allPath.api}/${route}/${id}`, {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken()
        }),
      })
      .then((r) => r.json())
        .then((response: any) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message));
        });
      });
  }

  /**
   * Realiza requisições externas.
   */
  public getAllExternal(route, obj) {
    return new Promise((resolve, reject) => {
      fetch(`${route}/${obj}`, {
        method: 'GET',
        headers: new Headers({
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }),
      })
      .then((r) => r.json())
        .then((response) => {
          const finalResponse = (response.data !== undefined) ? response.data : response;
          resolve(finalResponse);
        })
        .catch((e) => {
          reject(new Error(e.message));
        });
      });
  }

  /**
   * Realiza o download do arquivo específico.
   */
  public getFileForDownload(route, obj) {
    return new Promise((resolve, reject) => {
      fetch(`${route}/${obj}`, {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Bearer ' + this.getApiServiceToken(),
          'X-Requested-With': 'XMLHttpRequest'
        })
      }).then(res => res.blob())
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /**
   * Apagada os dados de login do usuário
   * e o manda para a tela de login.
   */
  public loggout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
