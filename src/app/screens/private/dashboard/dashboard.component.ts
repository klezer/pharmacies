import { Component, OnInit, AfterContentInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { RecipeCrudComponent } from './recipe-crud/recipe-crud.component';
import { ClientService } from './../../../services/client.service';
export interface RecipeObject {
  name: string;
  recipe_id: number;
  validity_of_recipe: string;
  date_generator: string;
  started_out: boolean;
  image: string;
}

export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ClientService]
})
export class DashboardComponent implements OnInit, AfterContentInit {
  public userProfile: any;
  public displayedColumns: string[] = ['recipe_id', 'started_out', 'name', 'date_generator', 'validity_of_recipe', 'document_file'];
  public dataSource = new MatTableDataSource<RecipeObject>();
  public routers: any = {
    retrieveAllRecipes: 'doctor-list-recipe',
    retrieveRecipeiD: 'doctor-recipe-patient',
    pdfUrl: 'https://api-drpediu.doc88.com.br/api/patient-generate-recipe-pdf/'
  };

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
              private clientService: ClientService) {
              }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngAfterContentInit(): void {
    this.userIsLogged();
  }

  /**
   * Verifica se o usuário está logado.
   */
  public userIsLogged() {
    this.clientService.isLogged().then((loggedInfo: string) => {
      if (loggedInfo) {
        this.userProfile = JSON.parse(loggedInfo);
        this.clientService.setApiServiceTokenApi(this.userProfile.token);
      }
    });
  }

  /**
   * Abre o PopUp para adição de nova receita.
   */
  public newRecipe(): void {
    const dialogRef = this.dialog.open(RecipeCrudComponent, {
      width: '600px',
      panelClass: 'modal-component-screen',
      data: { profileID: null, recipeInfo: null}
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.getRecipes();
    });
  }


  /**
   * Realiza o loggout do usuário.
   */
  public loggout() {
    this.clientService.loggout();
  }

}
