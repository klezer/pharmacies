import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { ClientService } from './../../../../services/client.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-recipe-crud',
  templateUrl: './recipe-crud.component.html',
  styleUrls: ['./recipe-crud.component.scss'],
  providers: [ClientService],
  encapsulation: ViewEncapsulation.None
})
export class RecipeCrudComponent implements OnInit {
  public delayTimer: any;
  public moment = moment;
  public dosageList: any = [];
  public frequencyList: any = [];
  public routers = {
      getAnamnesePatient: 'doctor-get-anamnese-patient',
      searchCPF: 'list-medicines-for-cpf',
      makeSale: 'sales-medicines',
      downloadPDF: 'https://api-drpediu.doc88.com.br/api/patient-generate-recipe-pdf'
  };
  public formRecipe: any = [];
  public formToSumbit = {
    doctor_id: '',
    user_id: '',
    type_recipe_id: 1,
    medicines: ''
  };
  public medicineList: any = [];
  public medicineItem: any = {
      dosage: null,
      considerations: null,
      frequency: null,
      number_of_days: null,
      title: null,
      subtitle: null,
      note: '',
      validity_of_medicine: null,
      opened: true
  };
  public canCreateRecipe = false;
  public patientProfile = {
    CPF: '',
    allProfile: null
  };
  public salesmanID = null;
  public patientID = null;
  public urlPDF = null;
  public showPDF = false;

  constructor(public clientService: ClientService,
              public dialogRef: MatDialogRef<RecipeCrudComponent>,
              @Inject( MAT_DIALOG_DATA) public data: any,
              private snackBar: MatSnackBar) {
      if (this.data.recipeInfo) {
        this.salesmanID = this.data.profileID;
        this.patientID = this.data.recipeInfo.medical_patient.id;
        this.formateAge(this.data.recipeInfo.medical_patient);
        this.getFileOfRecipe(this.data.recipeID);
      } else {
        this.userIsLogged();
      }
    }

    ngOnInit(): void {
      this.medicineList.push(this.medicineItem);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

  /**
   * Verifica se o usuário está logado.
   */
  public userIsLogged() {
    this.clientService.isLogged().then((loggedInfo: any) => {
      if (loggedInfo) {
        const userProfile = JSON.parse(loggedInfo);
        this.salesmanID = userProfile.id;
      }
    });
  }

  /**
   * Formata a data de nascimento.
   */
  public formateAge(obj) {
    const ageFormated = obj.date_of_birth.split('/').reverse().join('-');
    return obj.date_of_birth = (new Date(ageFormated));
  }

  /**
   * Adiciona o novo item do formulário validado.
   */
  public medicineForm(form) {
    if (form.new) {
        this.canCreateRecipe = true;
        this.formRecipe[form.index] = form.value;
        this.medicineList[form.index].medicine_id = form.value.medicine_id;
    }
    if (!form.new) {
        this.formRecipe.splice(form.index, 1);
        this.medicineList.splice(form.index, 1);
    }
  }

  /**
   * Valida o preenchimento do medicamento.
   */
  public validRecipe(valid) {
    this.canCreateRecipe = valid;
  }

  /**
   * Realiza a busca do CPF do Paciente.
   */
  public searchCPF() {
    this.clientService.getAll(this.routers.searchCPF, this.patientProfile.CPF).then((response: any) => {
      if (response.errors) {
        this.showMessage(response.error, 'error');
        } else {
          this.patientProfile.allProfile = response.patient;
          this.formateAge(this.patientProfile.allProfile);
          this.medicineList = response.medicines;
        }
    });
  }

  /**
   * Realiza a venda do medicamento.
   */
  public makeSale(item) {
    const sale = {
      medicine_id: item.medicine.id,
      user_id: this.salesmanID,
      title: item.medicine.title,
      dosage: item.medicine.dosage,
      frequency: item.medicine.frequency,
      number_of_days: item.medicine.number_of_days,
      validity_of_medicine: item.medicine.validity_of_medicine,
      subtitle: item.medicine.subtitle,
      note: item.medicine.note,
      doctor_name: item.doctor.name,
      doctor_cpf: item.doctor.cpf,
      doctor_crm_number: item.doctor.crm_number,
      patient_name: this.patientProfile.allProfile.name,
      patient_cpf: this.patientProfile.allProfile.cpf
    };
    this.clientService.postAll(this.routers.makeSale, sale, false).then((response: any) => {
      if (response.success) {
        item.medicine.sales_of_medicines_id = 1;
        this.showMessage(response.success, 'success');
      } else {
        this.showMessage(response.error, 'error');
      }
    });
  }

  /**
   * Realiza o download do arquivo da receita.
   */
  public getFileOfRecipe(recipeID) {
    this.clientService.getFileForDownload(this.routers.downloadPDF, recipeID).then((response: any) => {
      if (response.type === 'application/pdf') {
        this.urlPDF = URL.createObjectURL(response);
        this.showPDF = true;
      }
    });
  }

  /**
   * Exibe os erros de login da API.
   */
  public showMessage(msg, type) {
    this.snackBar.open(msg, '', {
      duration: 2000,
      panelClass: type
    });
  }

}
