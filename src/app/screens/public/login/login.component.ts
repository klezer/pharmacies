import { Component, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ClientService } from './../../../services/client.service';
import { MatSnackBar } from '@angular/material';
/** Erro quando o controle inválido está sujo, é tocado ou enviado. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ClientService, {provide: ErrorStateMatcher}]
})
export class LoginComponent {
  @HostBinding('class') public cssClass = 'screen-login';
  public loginForm: any;
  public matcher = new MyErrorStateMatcher();
  public hide = true;
  constructor(private router: Router,
              private clientService: ClientService,
              private snackBar: MatSnackBar) {
    this.loginForm  = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
    this.userIsLogged();
  }

  /**
   * Verifica se o usuário está logado.
   */
  public userIsLogged() {
    this.clientService.isLogged().then((logged) => {
      if (logged) {
        this.router.navigate(['/dashboard']);
      }
    });
  }

  /**
   * Submete os dados do formuário de login.
   */
  public submitLogin() {
    this.clientService.postAll('api-login', this.loginForm.value, true).then((response: any) => {
      if (response.success) {
        this.clientService.setUserProfile(JSON.stringify(response.success)).then((response));
        if (response) {
          this.router.navigate(['/dashboard']);
        }
      } else {
        this.errosMessage(response.errors);
      }
    });
  }

  /**
   * Exibe os erros de login da API.
   */
  public errosMessage(errors) {
    Object.keys(errors).forEach((field) => {
        errors[field].forEach((msg) => {
          this.snackBar.open(msg, '', {
            duration: 2000,
            panelClass: 'error'
          });
        });
    });
  }

}
