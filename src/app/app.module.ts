import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomMaterialModule } from './core/material.module';

// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { LoginComponent } from './screens/public/login/login.component';
import { DashboardComponent } from './screens/private/dashboard/dashboard.component';
import { RecipeCrudComponent } from './screens/private/dashboard/recipe-crud/recipe-crud.component';
// Formulário
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Tabela
import { MatPaginatorModule } from '@angular/material';
// Tooltip
import { MatTooltipModule } from '@angular/material/tooltip';
// Modal
import { MatDialogModule, MatNativeDateModule } from '@angular/material';
// Autocomplete
import { MatAutocompleteModule } from '@angular/material/autocomplete';
// Select
import { MatSelectModule } from '@angular/material/select';
// Slider
import { MatSliderModule } from '@angular/material/slider';
// Grid
import { MatGridListModule } from '@angular/material/grid-list';
// DatePicker
import { MatDatepickerModule, MAT_DATE_LOCALE } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
// Expansivo
import { MatExpansionModule } from '@angular/material/expansion';
// Máscara de campos
import { NgxMaskModule } from 'ngx-mask';
// Diretiva de tratamento de URL
import { SafeUrl } from './services/safe-url.service';
// SnackBar
import { MatSnackBarModule } from '@angular/material/snack-bar';
// Progress
import { MatProgressBarModule } from '@angular/material/progress-bar';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    RecipeCrudComponent,
    SafeUrl
  ],
  entryComponents: [RecipeCrudComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatSliderModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatProgressBarModule,
    NgxMaskModule.forRoot()
  ],
  providers: [MatDatepickerModule, {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
